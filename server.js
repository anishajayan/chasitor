require('dotenv').config()

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var Mitter = require('@mitter-io/node').Mitter;
var process = require('process');
var request = require('request');
const nlp = require('./utilities/nlp/index')
const DbClass = require('./utilities/db/index')
const db = new DbClass()
var app = express();
const port = process.env.PORT

var bot_created = false

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));



const mitter = Mitter.forNode(
    process.env.MITTER_APPLICATION_ID,
    {
      "accessKey":    process.env.MITTER_ACCESS_KEY,
      "accessSecret": process.env.MITTER_ACCESS_SECRET
    }
)

const userAuthClient = mitter.clients().userAuth()
const userClient = mitter.clients().users()
const channelClient = mitter.clients().channels()
const messageClient = mitter.clients().messages()

const Users = {
    't8r79-wTR2V-zyVhX-ZFa9H': {
        name: 'Santanu',
        credentials: {
            username: 'santanu',
            password: 'password'
        }
    },

    'user-001': {
        name: 'Santanu Bh',
        credentials: {
            username: 'santanuB',
            password: 'password'
        }
    },
  
    'mQuWv-hbQuN-pMdxd-1Otbh': {
        name: 'Anisha',
        credentials: {
            username: 'anisha',
            password: 'password'
        }
    },

    'mQuWv-hbQuN-pMdxd-1Otah': {
        name: 'Anish',
        credentials: {
            username: 'anish',
            password: 'password'
        }
    },

    'mQuzv-hbQuN-pMdxd-1Otah': {
        name: 'all',
        credentials: {
            username: 'all',
            password: 'password'
        }
    },

    'LuBHC-SAbpj-t9x10-GoMMf': {
        name: 'chasitor_bot'
    }
}

function loginSuccessfulResponse(res, userId, userToken, next) {
    res.status(200).send({
        mitterUserId: userId,
        mitterUserAuthorization: userToken,
        mitterAppId: process.env.MITTER_APPLICATION_ID
    })
    next
}

function createBotUser() {
    const createBot = userClient.createUser({
        userId: 'LuBHC-SAbpj-t9x10-GoMMf',
        systemUser: false,
        screenName: {
            screenName: 'chasitor_bot'
        }
    })
    .catch((e) => {
        if (!(e.response.status === 409 && e.response.data.errorCode === 'duplicate_entity')) {
            throw e
        }
    })

    createBot.then(() => {
        console.log('Bot created')
        bot_created = true
    })
    .catch((e) => {
        throw e
    })
}

function createBotChannel(res, userId, token) {
    if(!bot_created) {
        createBotUser()
    }

    let botChannelId = `bot-${userId}`;
    const createChannel = channelClient.newChannel({
        channelId: botChannelId,
        defaultRuleSet: 'io.mitter.ruleset.chats.DirectMessage',
        participation: [
            {
                participantId: 'LuBHC-SAbpj-t9x10-GoMMf',
                participationStatus: 'Active'
            },
            {
                participantId: userId,
                participationStatus: 'Active'
            }
        ],
        systemChannel: false,
        entityProfile: {
            entityId: {
                identifier: botChannelId
            },
            attributes: [
                {
                    key: 'mitter.cpa.DisplayName',
                    contentType: '',
                    contentEncoding: 'text/json',
                    value: 'MovieMate'
                }
            ]
        }
    })
    .catch((e) => {
        console.log('channel creation error!!')
        console.log(e)
        throw e
    })

    createChannel.then(() => {
        console.log('Bot channel created')
        loginSuccessfulResponse(res, userId, token.userToken.signedToken)
        sendBotMessage(botChannelId,"Hi! I am your MovieMate and can help you with movie suggestions or queries. Just search 'Show me movies of Shah rukh khan'")
    }).catch(e => {throw e})
}

function sendBotMessage(channelId, message) {
    let today = new Date();
    let sendMessage = messageClient.sendMessage(channelId, {
        payloadType: 'mitter.mt.Text',
        senderId: 'LuBHC-SAbpj-t9x10-GoMMf',
        textPayload: message,
        timelineEvents: [
            {
                type: 'mitter.mtet.SentTime',
                eventTimeMs: today.getTime(),
                subject: 'LuBHC-SAbpj-t9x10-GoMMf'
            }
        ]
    })
    .catch((e) => {
        // console.log(e)
        throw e
    })

    sendMessage.then(() => {console.log('Bot replied')}).catch((e) => {throw e})
}

app.post('/login', async function(req, res, next) {
    const { username, password } = req.body;

    // const userFound = Object.keys(Users).find((userId) => {
    //     const { username: targetUsername, password: targetPassword } = Users[userId].credentials;
    //     return username === targetUsername && password === targetPassword;
    // })
    db.getUser(username, password)
    .then((userFound) => {

        if (userFound) {
            const getUser = userClient.getUser(userFound).catch((e) => { throw e })

            getUser.then(() => userAuthClient.getUserToken(userFound))
            .then(token => {
                loginSuccessfulResponse(res, userFound, token.userToken.signedToken)
            })
            .catch(e => {
                console.error('Error executing request, setting 500', e)
                res.sendStatus(500)
            })
        } else {
            res.sendStatus(401);
            return Promise.resolve();
        }
    })
    .catch((err) => console.log(err))
});

app.post('/register', function(req, res) {
    const { username, password } = req.body;

    // const userFound = Object.keys(Users).find((userId) => {
    //     const { username: targetUsername, password: targetPassword } = Users[userId].credentials;
    //     return username === targetUsername && password === targetPassword;
    // })
    db.insertUser(username, password)
    .then((userFound) => {
        console.log("ID-",userFound)

        if (userFound) {
            const createUser = userClient.createUser({
                userId: userFound,
                userLocators: [],
                systemUser: false,
                screenName: {
                    screenName: username
                }
            })
            .catch((e) => {
                if (!(e.response.status === 409 && e.response.data.errorCode === 'duplicate_entity')) {
                    throw e
                }
            })
      
            createUser.then( () => userAuthClient.getUserToken(userFound))
                .then(token => {
                    createBotChannel(res, userFound, token)
                    
                })
                .catch(e => {
                    console.log('hey')
                    console.error('Error executing request, setting 500', e)
                    res.sendStatus(500)
                })
        } else {
            res.sendStatus(401);
            return Promise.resolve();
        }
    })
    .catch((err) => console.log(err.message))
    
});

app.post('/chatbot', function(req, res) {
    let { message, channelId, expect_input, suggestion_asked, bot_context } = req.body
    nlp.processText(channelId, message, expect_input, suggestion_asked, bot_context, (expect_input, suggest, bot_context) => {
        res.status(200).send([expect_input, suggest, bot_context])
    })
})

app.get('*',(request,response) =>{
    response.sendFile(path.join(__dirname + '/public/views/index.html'));
});

app.listen(port, () => console.log(`Listening to port ${port}`))