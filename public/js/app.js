'use strict';
const app = angular.module('app', ['ngRoute','ui.bootstrap', 'emguo.poller']);

app.config(function ($routeProvider, $locationProvider, pollerConfig) {
    $routeProvider
        .when('/', {
            templateUrl: '/views/pages/login.html',
            controller: 'loginController'
        })
        .when('/user/:username', {
            templateUrl: '/views/pages/chat.html',
            controller: 'chatController'
        });
    $locationProvider.html5Mode(true);

    //Stop all pollers when navigating between views
    pollerConfig.stopOn = '$routeChangeStart';
});

// app.factory('appService', ($http) => {
//     return new AppService($http)
// });
