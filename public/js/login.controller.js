'user strict';

app.controller('loginController', function ($rootScope, $scope, $location, $timeout, $http, $window) {
    if($window.localStorage.getItem("username")){
        console.log(`/user/${$window.localStorage.getItem("username")}`)
        $location.path(`/user/${$window.localStorage.getItem("username")}`);
    } else {
        $rootScope.user = {
            username: '',
            password: '',
            cnfpassword: '',
            mitterUserID: '',
            mitterUserAuth: ''
        }
    };
    

    $scope.urls = {
        login: '/login',
        register: '/register'
    }

    /* usernamme check variables starts*/
    let TypeTimer;
    const TypingInterval = 800;
    /* usernamme check variables ends*/

    $scope.clearUser = () => {
        $rootScope.user = {
            username: '',
            password: '',
            cnfpassword: '',
            mitterUserID: '',
            mitterUserAuth: ''
        };
        console.log(JSON.stringify($rootScope.user))
    }
    
    $scope.checkIfLoggedInUser = () => {
      
    }

    $scope.authUser = (authType) => {
        if (authType === 'register' && $rootScope.user.password != $rootScope.user.cnfpassword) {
            return
        }

        $http({
            method: 'POST',
            url: $scope.urls[authType],
            headers: {
                'Content-Type': 'application/json'
              },
            data: {
                    username: $rootScope.user.username,
                    password: $rootScope.user.password
                }
        }).then(
            function(success){
                $rootScope.user.mitterUserAuth = success.data.mitterUserAuthorization
                $rootScope.user.mitterUserID = success.data.mitterUserId
                $rootScope.user.mitterAppId = success.data.mitterAppId
                $window.localStorage.setItem("username",$rootScope.user.username);
                $window.localStorage.setItem("mitterUserID",$rootScope.user.mitterUserID);
                $window.localStorage.setItem("mitterUserAuth",$rootScope.user.mitterUserAuth);
                $window.localStorage.setItem("mitterAppId",$rootScope.user.mitterAppId);
                $location.path(`/user/${$rootScope.user.username}`);
            },
            function(error){
                console.log("Error in authUser: " +JSON.stringify(error))
                // if(authType == 'login')
                //     alert('User not registered')
                // else
                //     alert('User registration failed')
                $rootScope.user.mitterUserID =  $window.localStorage.getItem("mitterUserID");
                $rootScope.user.mitterUserAuth = $window.localStorage.getItem("mitterUserAuth");
            }
        );
    }
});