'user strict';

app.controller('chatController', function ($rootScope, $scope, poller, $location, $http, $window, $timeout){
    console.log('in getChannels---'+$window.localStorage.getItem("mitterUserID"))
    $scope.typing = false;
    $rootScope.user = {
        username: $window.localStorage.getItem("username"),
        mitterUserID: $window.localStorage.getItem("mitterUserID"),
        mitterUserAuth: $window.localStorage.getItem("mitterUserAuth"),
        mitterAppId: $window.localStorage.getItem("mitterAppId"),
        obj: ''
    };
    const UserId = $rootScope.user.mitterUserID
    $scope.data = {
        username: '',
        chatlist: [],
        selectedFriendId: null,
        selectedFriendName: null,
        messages: [],
        users: []
    };

    $scope.bot_config = {
        expect_input: false,
        suggestion_asked: false,
        bot_context: null
    }
    $scope.clearNewGroupChatForm = () => {
        $scope.chat_group_name = ""
        $scope.group_participant_list = []
        $scope.checked = false
        if($scope.data.users.length>0){
          angular.forEach($scope.data.users, function(obj) {
              $scope.editParticipationList($scope.checked,obj)
        
          });
        }
      
    }

    
    $scope.message_fetch_query = ""
    $scope.loadPromise;

    $scope.max_scroll_top = 0
    $scope.auto_scroll = true

    $scope.clearNewGroupChatForm();

    console.log($rootScope.user.username)
    $scope.selectedChannelParticipants = []
    $scope.getUsers = () =>{
        $http({
            method: 'GET',
            url: 'https://api.mitter.io/v1/users?sandboxed=true',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json;charset=UTF-8',
                'X-Mitter-Application-Id': $rootScope.user.mitterAppId,
                'X-Issued-Mitter-User-Authorization': $rootScope.user.mitterUserAuth
            },
        }).then(
            function(res){
                //console.log("getUsers: "+JSON.stringify(res.data))
                $scope.data.users = res.data
                const myData = $scope.data.users.filter((obj) => {
                    return obj.userId === $rootScope.user.mitterUserID
             
                });
                $scope.data.users = []
                angular.forEach(res.data, function(obj) {
                    
                    if(obj.userId === $rootScope.user.mitterUserID){
                        $rootScope.user.obj = obj
                    }else{
                        if(obj.userId != 'LuBHC-SAbpj-t9x10-GoMMf') {
                            this.push(obj);
                        }
                    }
                        
                  
                }, $scope.data.users);
            },
            function(error){
                console.log("error: "+JSON.stringify(error))
                return []
            }
        ).catch((error) => {
            alert(error.message);
        });
    }
    $scope.getUsers();
    
    $scope.createChannel = (friend) =>{
        var groupType = ''
        if(friend) {
            $scope.group_participant_list.push(friend)
            var channelName = friend.screenName.screenName + $rootScope.user.username
            groupType = 'io.mitter.ruleset.chats.DirectMessage'
        } else {
            var channelName = $scope.chat_group_name
            groupType = 'io.mitter.ruleset.chats.GroupChat'
        }
        var channelId = btoa(Math.random()).substring(0,20)
        
        var data = {
            channelId: channelId,
            defaultRuleSet: groupType,
            participation: [
                {
                    participantId: $rootScope.user.obj.userId,
                    participationStatus: 'Active'
                }
            ],
            systemChannel: false,
            entityProfile: {
                entityId: {
                    identifier: channelId
                },
                attributes: [
                    {
                        key: 'mitter.cpa.DisplayName',
                        contentType: '',
                        contentEncoding: 'text/json',
                        value: channelName
                    }
                ]
            }
        }
        for(let participant of $scope.group_participant_list) {
            if(participant)
                data.participation.push({participantId: participant.userId, participationStatus: 'Active'})
        }
        $http({
            method: 'POST',
            url: 'https://api.mitter.io/v1/channels',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json;charset=UTF-8',
                'X-Mitter-Application-Id': $rootScope.user.mitterAppId,
                'X-Issued-Mitter-User-Authorization': $rootScope.user.mitterUserAuth
            },
            data: data
        }).then(
            function(res){
                //console.log("createChannel: "+JSON.stringify(res.data))
                const channelId = res.data.identifier;
                //console.log("channelId: "+channelId)
                if(friend) {
                    $scope.participants.push(friend.userId);
                    data.entityProfile.attributes[0].value = data.entityProfile.attributes[0].value.replace($rootScope.user.username, "")
                } else {
                    // for(let participant of $scope.group_participant_list) {
                    //     if(participant)
                    //         $scope.participants.push(participant.userId)
                    // }
                    $scope.clearParticipationList()
                }
                $scope.data.chatlist.splice(0, 0, data)
                $scope.selectFriendToChat(channelId);
                $scope.active = 0;
                $('#exampleModal').modal('hide')
                // $('#exampleModal').modal('dispose')
            },
            function(error){
                console.log("error: "+JSON.stringify(error))
                return []
            }
        ).catch((error) => {
            alert(error.message);
        });
    }
    // $scope.getChannelMessages = (channelId) =>{
    //   $http({
    //       method: 'GET',
    //       url: 'https://api.mitter.io/v1/channels/'+channelId+'/messages',
    //       headers: {
    //         'Content-Type': 'application/json',
    //         'X-Issued-Mitter-User-Authorization': $rootScope.user.mitterUserAuth
    //       },
    //   }).then(
    //       function(res){
    //           //console.log("in get channels: "+JSON.stringify(res.data))
    //         angular.forEach(res.data, function(value) {
    //                 // this.push({message: value["textPayload"], fromUserId: value["senderId"]["identifier"]});
    //                 try {
    //                     value.textPayload = JSON.parse(value.textPayload)
    //                     this.push(value)
    //                 } catch(e) {
    //                     this.push(value)
    //                 }
              
    //         }, $scope.data.messages);
    //         $scope.data.messages = $scope.data.messages.reverse()
    //       },
    //       function(error){
    //           console.log("error: "+error)
    //           return []
    //       }
    //   ).catch((error) => {
    //       alert(error.message);
    //   });
        

    // }
    
    $scope.getChannelMessages = (channelId) =>{
        poller.stopAll()
        $scope.data.messages =[]
        let first = true
        url = 'https://api.mitter.io/v1/channels/' + channelId + '/messages'
        var messagePoller = poller.get(url, {
            action: 'get',
            delay: 800,
            argumentsArray: [
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Issued-Mitter-User-Authorization': $rootScope.user.mitterUserAuth
                    }
                }
            ],
            smart: true
        });
    
        messagePoller.promise.then(null, null,
            function(res) {
                
                let i = 0
                console.log("demo",res.data)
                let prev_length = $scope.data.messages.length
                while(i<res.data.length) {
                    if(prev_length > 0 && !first && res.data[i].messageId == $scope.data.messages[prev_length-1].messageId)
                        break
                    else {
                        try {
                            res.data[i].textPayload = JSON.parse(res.data[i].textPayload)
                        } catch(e) {}
                        if($scope.data.messages.length == 0)
                            $scope.data.messages.push(res.data[i])
                        else if(first)
                            $scope.data.messages.splice(0,0,res.data[i])
                        else
                        $scope.data.messages.splice(prev_length,0,res.data[i])
                        
                        i += 1
                    }
                }
                first = false
                // const messageThread = document.querySelector('.message-thread');
                // // console.log('top-',messageThread.scrollTop,'   prev-',$scope.prev_scroll_top)
                // if($scope.prev_scroll_top - messageThread.scrollTop <= 35){
                //     $scope.auto_scroll = true
                // } else {
                //     $scope.auto_scroll = false
                // }
                // if($scope.auto_scroll) {
                //     messageThread.scrollTop = messageThread.scrollHeight + 500;
                // }
                // if($scope.prev_scroll_top == 0) {
                //     $scope.prev_scroll_top = messageThread.scrollTop
                // }
                // poller.stopAll()
            }
        ).catch((error) => {
            alert(error.message);
        });
    }
    $scope.participants = []
    $scope.getChannelParticipations = (channelId,all_or_single) => {
        //console.log('in getChannelParticipations')
        $http({
            method: 'GET',
            url: 'https://api.mitter.io/v1/channels/'+channelId+'/participants',
            headers: {
              'Content-Type': 'application/json',
              'X-Issued-Mitter-User-Authorization': $rootScope.user.mitterUserAuth
            },
        }).then(
            function(res){
                //console.log("in get channels: "+JSON.stringify(res.data))
                angular.forEach(res.data, function(value) {
                        this.push(value["participantId"]["identifier"]);
                        let botChannelId = `bot-${UserId}`;
                        if(all_or_single == "singleChannelParticipants" && botChannelId !== channelId){
                            $scope.selectedChannelParticipants = ["You"]
                            $scope.selectedChannelParticipants.push(value["participant"]["screenName"]["screenName"])
                        }else if(botChannelId === channelId){
                          $scope.selectedChannelParticipants = []
                        }
                }, $scope.participants);
                  //console.log("$scope.participants: "+$scope.participants.length)
                
            },
            function(error){
                console.log("error: "+error)
                return []
            }
        ).catch((error) => {
            alert(error.message);
        });
    }
    
    $scope.getChannels = () =>{
        //console.log('in getChannels')
        $http({
            method: 'GET',
            url: 'https://api.mitter.io/v1/users/' + $rootScope.user.mitterUserID + '/channels',
            headers: {
              'Content-Type': 'application/json',
              'X-Issued-Mitter-User-Authorization': $rootScope.user.mitterUserAuth
            },
        }).then(
            function(res){
                //console.log("in get channels: "+res.data)
                // $scope.$apply();
                angular.forEach(res.data, function(value) {
                    var channelId = value["channel"]["channelId"];
                    value['channel']['entityProfile']['attributes'][0]['value'] = value['channel']['entityProfile']['attributes'][0]['value'].replace($rootScope.user.username, "")
                    console.log(value["channel"])
                        this.push(value["channel"]);
                        $scope.getChannelParticipations(channelId,"allParticipants");
                }, $scope.data.chatlist);
                if($scope.data.chatlist.length>0 && !$scope.data.selectedFriendId){
                  //console.log("defult channel: "+JSON.stringify($scope.data.chatlist[0]))
                    var firstChannelId = $scope.data.chatlist[0]["channelId"]
                    $scope.selectFriendToChat(firstChannelId)
                }
                  
            },
            function(error){
                console.log("error: "+error)
                return []
            }
        ).catch((error) => {
            alert(error.message);
        });
        
    }
    $scope.getChannels();
    
    
    $scope.selectFriendToChat = (channelId) => {
        /*
        * Highlighting the selected user from the chat list
        */
        const friendData = $scope.data.chatlist.filter((obj) => {
            return obj.channelId === channelId;
        });
        $scope.data.selectedFriendName = friendData[0]["entityProfile"]["attributes"][0]["value"]
        $scope.data.selectedFriendId = channelId;
        $scope.data.messages = []
        $scope.getChannelMessages(channelId)
        $scope.getChannelParticipations(channelId,"singleChannelParticipants");
        /**
        * This HTTP call will fetch chat between two users
        */
        // appService.getMessages(UserId, channelId).then( (response) => {
        //     $scope.$apply(() => {
        //         $scope.data.messages = response.messages;
        //     });
        // }).catch( (error) => {
        //     console.log(error);
        //     alert('Unexpected Error, Contact your Site Admin.');
        // });
    }
    
    $scope.sendMessage = (event) => {

        if (event.keyCode === 13) {

            let toUserId = null;
            let toSocketId = null;

            /* Fetching the selected User from the chat list starts */
            let selectedFriendId = $scope.data.selectedFriendId;
            if (selectedFriendId === null) {
                return null;
            }
            friendData = $scope.data.chatlist.filter((obj) => {
                return obj.channelId === selectedFriendId;
            });
            /* Fetching the selected User from the chat list ends */
            
            /* Emmiting socket event to server with Message, starts */
            if (friendData.length > 0) {

                toUserId = friendData[0]['id'];
                toSocketId = friendData[0]['socketid'];            

                let messagePacket = {
                    message: document.querySelector('#message').value,
                    fromUserId: UserId,
                    toUserId: toUserId,
                    toSocketId: toSocketId
                };
                var data = {
                            payloadType: "mitter.mt.Text",
                            senderId: {identifier: $rootScope.user.mitterUserID},
                            textPayload: messagePacket.message.trim()
                        }
                
                // $scope.data.messages.push(data);
                var today = new Date();
                $http({
                    method: 'POST',
                    url: 'https://api.mitter.io/v1/channels/' + friendData[0].channelId + '/messages',
                    headers: {
                      'Content-Type': 'application/json',
                      'X-Issued-Mitter-User-Authorization': $rootScope.user.mitterUserAuth
                    },
                    data: {
                            payloadType: "mitter.mt.Text",
                            senderId: $rootScope.user.mitterUserID,
                            textPayload: messagePacket.message.trim(),
                            timelineEvents:[
                               {
                                    type: "mitter.mtet.SentTime",
                                    eventTimeMs: today.getTime(),
                                    subject: $rootScope.user.mitterUserID
                                }
                            ]
                        }
                }).then(
                    function(res){
                        //console.log(res.data)
                        if(friendData[0].channelId == `bot-${$rootScope.user.mitterUserID}`) {
                            $scope.typing = true;
                            $http({
                                method: 'POST',
                                url: '/chatbot',
                                headers: {
                                    'Content-Type': 'application/json'
                                },
                                data: {
                                    message: messagePacket.message.trim(),
                                    channelId: friendData[0].channelId,
                                    expect_input: $scope.bot_config.expect_input,
                                    suggestion_asked: $scope.bot_config.suggestion_asked,
                                    bot_context: $scope.bot_config.bot_context
                                }
                            }).then((res) => {
                                $scope.typing = false;
                                $scope.bot_config.expect_input = res.data[0]
                                $scope.bot_config.suggestion_asked = res.data[1]
                                $scope.bot_config.bot_context = res.data[2]
                            }, (error) => console.log(error))
                        }
                        // $scope.$apply();
                        // angular.forEach(res.data, function(value) {
                        //     console.log("key:"+value["channel"]["channelId"])
                        //     if(value["channel"]["entityProfile"]["attributes"][0]["value"]){
                        //         this.push(value["channel"]["entityProfile"]["attributes"][0]["value"]);
                        //     }else{
                        //         this.push(value["channel"]["channelId"]);
                        //     }
                        //
                        // }, $scope.messages);
                
                    },
                    function(error){
                        console.log("error: "+JSON.stringify(error))
                        return []
                    }
                ).catch((error) => {
                    alert(error.message);
                });

                document.querySelector('#message').value = '';
                //appService.scrollToBottom();
            }else {
                alert('Unexpected Error Occured,Please contact Admin');
            }
            /* Emmiting socket event to server with Message, ends */
        }
    }

    $scope.alignMessage = (fromUserId) => {
        return fromUserId == UserId ? true : false;
    }

    $scope.logout = () => {
        $window.localStorage.removeItem("mitterUserID");
        $window.localStorage.removeItem("mitterUserAuth");
        $window.localStorage.removeItem("mitterAppId");
        $window.localStorage.removeItem("username");
        //appService.socketEmit(`logout`, UserId);
        $location.path(`/`);
    }

    $scope.checkMessageType = (message) => {
        // console.log("in checkMessageType "+typeof message)
        return typeof message
    }

    $scope.editParticipationList = (checked, participant) => {
        if(checked) {
            $scope.group_participant_list.push(participant)
        } else {
            delete $scope.group_participant_list[$scope.group_participant_list.indexOf(participant)]
        }
        console.log("$scope.group_participant_lis: "+$scope.group_participant_list.length)
    }

    $scope.clearParticipationList = () => {
        $scope.group_participant_list = []
        $scope.checked = false
        $scope.chat_group_name = ""
    }
});