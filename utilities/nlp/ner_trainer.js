const { NerManager } = require('node-nlp');
const yaml = require('require-yml')
const fs = require('fs');

const manager = new NerManager({ threshold: 0.8 });

const traiing_file = yaml('./ner_training_list')

for( var ner_set in traiing_file) {
    ner_data = traiing_file[ner_set]
    for(let i=0; i < ner_data.length; i++) {
        // console.log(ner_data[i])
        if(typeof ner_data[i] == 'string') {
            manager.addNamedEntityText(
                ner_set,
                ner_data[i],
                ['en'],
                [ner_data[i]],
            );
        } else if(typeof ner_data[i] == 'object') {
            key = Object.keys(ner_data[i])[0]
            manager.addNamedEntityText(
                ner_set,
                key,
                ['en'],
                ner_data[i][key],
            );
        }
    }
}

fs.writeFileSync('model.ner', JSON.stringify(manager.save()), 'utf8');


// manager.findEntities('Some christopher nolan movie').then((res) => console.log(res))

module.exports = manager