const { NlpManager } = require('node-nlp');
 
const manager = new NlpManager({ languages: ['en'] });

/*-------------------Greetings--------------------*/
manager.addDocument('en', 'goodbye for now', 'greetings.bye');
manager.addDocument('en', 'bye bye take care', 'greetings.bye');
manager.addDocument('en', 'see you later', 'greetings.bye');
manager.addDocument('en', 'bye for now', 'greetings.bye');
manager.addDocument('en', 'i must go', 'greetings.bye');
manager.addDocument('en', 'hello', 'greetings.hello');
manager.addDocument('en', 'hi', 'greetings.hello');
manager.addDocument('en', 'hey', 'greetings.hello');
manager.addDocument('en', 'howdy', 'greetings.hello');

manager.addDocument('en', 'suggest me a movie', 'suggest.general');
manager.addDocument('en', 'movies', 'suggest.general');
manager.addDocument('en', 'i want to watch a movie today', 'suggest.general');
manager.addDocument('en', 'suggest me movies of', 'suggest.general');

manager.addDocument('en', 'yes', 'reply.positive');
manager.addDocument('en', 'yep', 'reply.positive');

manager.addDocument('en', 'no', 'reply.negative');
manager.addDocument('en', 'nope', 'reply.negative');
manager.addDocument('en', 'none', 'reply.negative');
manager.addDocument('en', 'negative', 'reply.negative');

manager.addDocument('en', 'suggest some of his movies', 'context.retain');
manager.addDocument('en', 'suggest some of her movies', 'context.retain');
manager.addDocument('en', 'suggest some movies of this actor', 'context.retain');
manager.addDocument('en', 'suggest some movies of this person', 'context.retain');
manager.addDocument('en', 'suggest some movies of this actress', 'context.retain');
manager.addDocument('en', 'suggest some movies of same actress', 'context.retain');
manager.addDocument('en', 'suggest some movies of same actor', 'context.retain');
manager.addDocument('en', 'suggest some movies of same person', 'context.retain');

manager.addDocument('en', 'suggest movies of different actress', 'context.destroy');
manager.addDocument('en', 'suggest movies of different actor', 'context.destroy');
manager.addDocument('en', 'suggest movies of different person', 'context.destroy');
manager.addDocument('en', 'suggest movies of some other actress', 'context.destroy');
manager.addDocument('en', 'suggest movies of some other actor', 'context.destroy');
manager.addDocument('en', 'suggest movies of some other person', 'context.destroy');
manager.addDocument('en', 'suggest movies of someone else', 'context.destroy');

manager.addDocument('en', 'tell me about him', 'context.about');
manager.addDocument('en', 'tell me about her', 'context.about');

manager.addDocument('en', 'search', 'context.search');


 
// Train also the NLG
manager.addAnswer('en', 'greetings.bye', 'Till next time');
manager.addAnswer('en', 'greetings.bye', 'see you soon!');
manager.addAnswer('en', 'greetings.hello', 'Hey there!');
manager.addAnswer('en', 'greetings.hello', 'Hi!');

manager.addAnswer('en', 'suggest.general', 'Do you have any preference?');
manager.addAnswer('en', 'suggest.general', 'Any specific Genre or Actor in your mind?');

manager.addAnswer('en', 'reply.positive', 'Please go ahead');
manager.addAnswer('en', 'reply.positive', 'Please go on');
manager.addAnswer('en', 'reply.positive', 'Okay');

manager.addAnswer('en', 'reply.negative', 'Okay');
 
// Train and save the model.
manager.train();
manager.save();

// manager.process('en', 'bye').then(console.log);

module.exports = manager