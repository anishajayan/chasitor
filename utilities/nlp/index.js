const fs = require('fs');
const axios = require('axios');
const process = require('process');
const { NlpManager } = require('node-nlp');
const { NerManager } = require('node-nlp');
const ChatApi = require('../api/index')
const chat = new ChatApi()


const nerManager = new NerManager();
const nlpManager = new NlpManager({ languages: ['en'] });

/*------Load Models------*/
const ner_data = fs.readFileSync('./utilities/nlp/model.ner', 'utf8');
var saved_ner_data = JSON.parse(ner_data)

nerManager.load(saved_ner_data)
nlpManager.load('./utilities/nlp/model.nlp')

const tmdb_api_key = process.env.TMDB_ACCESS_KEY

const genres= {
    "Action": 28,
    "Adventure": 12,
    "Animation": 16,
    "Comedy": 35,
    "Crime": 80,
    "Documentary": 99,
    "Drama": 18,
    "Family": 10751,
    "Fantasy": 14,
    "History": 36,
    "Horror": 27,
    "Music": 10402,
    "Mystery": 9648,
    "Romance": 10749,
    "Science Fiction": 878,
    "TV Movie": 10770,
    "Thriller": 53,
    "War": 10752,
    "Western": 37
}

var channelId = ''
var message = ''
var expect_user_input = false
var suggestion_asked = false
var movie_discovered = false

var context = {
    people: [],
    genre: [],
    latest: false,
    movie: []
}
var nerIdentification = {
    people: [],
    genre: [],
    latest: false,
    movie: []
}

var nlp_res = null
var ner_res = null

var peopleId = []
var genreId = []
var peopleBio = []
var movieDetails = []

var search = false
var search_for = null
var people_in_search_result = 0

async function nerTextProcessing() {
    await nerManager.findEntities(message)
    .then((resp) => {
        for(var i = 0; i < resp.length; i++) {
            ner = resp[i]
            if(ner['entity'] === 'people') {
                if(ner['accuracy'] < 0.98) {
                    //return Couldn't find person, search for this person instead
                }
                context.people.push(ner['option'])
                nerIdentification.people.push(ner['option'])
            }
            else if(ner['entity'] === 'genre') {
                context.genre.push(ner['option'])
                nerIdentification.genre.push(ner['option'])
            }
            else if(ner['entity'] === 'latest') {
                context.latest = true
                nerIdentification.latest = true
            }
            else if(ner['entity'] === 'movie') {
                context.movie.push(ner['option'])
                nerIdentification.movie.push(ner['option'])
                console.log('root check-',context.movie)
            }
        }
        // if(context.people.length != 0 || context.genre.length != 0) {
        //     expect_user_input = false
        // }

        // callback(resp)
        ner_res = resp
        console.log('ended ner')
    });
}

async function nlpTextProcessing() {
    await nlpManager.process('en', message)
    .then((res) => {
        if(res['intent'].includes('suggest') || res['intent'].includes('about') || res['intent'].includes('retain')) {
            suggestion_asked = true
        }

        if(!expect_user_input) {
            if(res['intent'].includes('suggest')) {
                expect_user_input = true
            }

            if(!suggestion_asked){
                if(res['intent'] === 'reply.negative' || res['intent'] === 'reply.positive') {
                    delete res['answer']
                } else if (res['intent'] === 'context.search') {
                    // search_for = /("([^"])*")/g.exec(message)
                    search_for = message.match(/("([^"])*")/g)
                    console.log(search_for)
                    if(search_for && search_for.length == 1) {
                        search = true
                    } else if (search_for && search_for.length > 1) {
                        res.answer = 'Sorry, I can only search for one keyword at a time.'
                    } else {
                        res.answer = 'Nothing to search.'
                    }
                }
            }
        } else if (expect_user_input) {
            if(res['intent'] === 'reply.negative') {console.log(message)
                expect_user_input = false
            }
        }
        

        if(!('answer' in res)) {
            res.answer = 'Sorry, I could not get that.'
        }
        // sendBotMessage(channelId, res['answer'])
        // callback(res)
        nlp_res = res
        console.log('ended nlp')
    });
}

function formatPeople(person, person_more_details) {
    let known_for_movies_list = []
    let person_data = {}
    for(movie of person.known_for) {
        if(movie['media_type'] == 'movie') {
            saveName(movie['original_title'])

            let famous_movies = {
                'Title': movie['original_title'],
            }
            if(movie['genre_ids'] > 0) {
                famous_movies['Genre'] = genreIdToName(movie['genre_ids']).join(', ')
            }
            if(movie['vote_average']) {
                famous_movies['Rating'] = movie['vote_average']
            }
            known_for_movies_list.push(famous_movies)
        }

    }
    
    if(person.profile_path) {
        person_data = {
            profile_path: 'http://image.tmdb.org/t/p/w92' + person.profile_path,
            'Name': person.name
        }
    } else {
        person_data = {
            Name: person.name
        }
    }

    if(person_more_details && person_more_details.data.biography && person_more_details.data.biography != "") {
        person_data['Biography'] = person_more_details.data.biography
    }
    if(person_more_details && person_more_details.data.birthday && person_more_details.data.birthday != "") {
        person_data['Birthday'] = person_more_details.data.birthday
    }
    if(person_more_details && person_more_details.data.deathday && person_more_details.data.deathday != "") {
        person_data['Deathday'] = person_more_details.data.deathday
    }
    if(known_for_movies_list.length > 0) {
        person_data['Famous for'] = known_for_movies_list
    }

    return person_data
}

async function getPeople(discovery) {
    expect_user_input = false

    for(let person of context.people) {
        let query = ('&query=' + person).replace(' ', '%20')
        await axios({
            "method": "GET",
            "url": "https://api.themoviedb.org/3/search/person?include_adult=false&page=1&language=en-US&api_key="+ tmdb_api_key + query,
            "headers": {}
        })
        .then(async (res) => {
            if(discovery && res.data.results.length > 0) {
                peopleId.push(res.data.results[0].id)
            } else if(!discovery) {
                if(res.data.results.length > 0) {
                    await axios({
                        method: "GET",
                        url: "https://api.themoviedb.org/3/person/" + res.data.results[0].id + "?api_key=" + tmdb_api_key +"&language=en-US",
                        "headers": {}
                    })
                    .then((people_res) => {
                        peopleBio.push(formatPeople(res.data.results[0], people_res))
                    })
                }
            }
            // return res.data.results[0]
        })
        .catch((e) => {
            console.log(e.response)
            throw e
        })
    }
}

async function discoverMovie(callback) {
    expect_user_input = false

    await Promise.all([getPeople(true)])
    .then(() => {
        var url = "https://api.themoviedb.org/3/discover/movie?include_adult=false&language=en-US&api_key=" + tmdb_api_key
        
        if(peopleId.length > 0) {
            let with_people = '&with_people=' + peopleId.join('%7C')
            url += with_people
        }
        if(context.latest) {
            url += '&sort_by=release_date.desc'
        } else {
            url += '&sort_by=vote_average.desc'
        }
        if(context.genre.length > 0) {
            genreNameToId()
            let with_genres = '&with_genres=' + genreId.join('%7C')
            url += with_genres
        }
        // let with_genres = '&with_genres=' + context.genre.join(',')
        console.log('url',url)

        axios({
            "method": "GET",
            "url": url,
            "headers": {}
        })
        .then((res) => {
            callback(res.data)
        })
        .catch((e) => {
            throw e
        })
    })
    console.log(peopleId)
}

function saveName(movie_name) {
    nerManager.findEntities(movie_name)
    .then((resp) => {
        for(var i = 0; i < resp.length; i++) {
            let entity = resp[i]
            if(entity['entity'] === 'movie') {
                return
            }
        }

        if(!saved_ner_data.namedEntities.hasOwnProperty('movie')) {
            saved_ner_data.namedEntities.movie = {
                type: 'enum',
                name: 'movie',
                localeFallback: {
                    '*': 'en'
                },
                locales: {
                    en: {}
                }
            }
        }
        saved_ner_data.namedEntities.movie.locales.en[movie_name] = [movie_name]
        movie_discovered = true
    })
}

async function getMovieDetails() {
    expect_user_input = false

    for(let movie of context.movie) {
        let query = ('&query=' + movie).replace(' ', '%20')
        await axios({
            "method": "GET",
            "url": "https://api.themoviedb.org/3/search/movie?api_key=" + tmdb_api_key + "&language=en-US&query=" + query + "&page=1&include_adult=false",
            "headers": {}
        })
        .then(async (res_with_id) => {
            let movieID = res_with_id.data.results[0].id

            await axios({
                method: "GET",
                url: "https://api.themoviedb.org/3/movie/" + movieID +"?api_key=" + tmdb_api_key + "&language=en-US"
            })
            .then((res) => {
                if(res.data.poster_path) {
                    var movie_data = {
                        poster_path: 'http://image.tmdb.org/t/p/w92' + res.data.poster_path,
                        Title: res.data.title
                    }
                } else if(res.data.backdrop_path) {
                    var movie_data = {
                        poster_path: 'http://image.tmdb.org/t/p/w92' + res.data.backdrop_path,
                        Title: res.data.title
                    }
                } else {
                    var movie_data = {
                        Title: res.data.title
                    }
                }
                if(res.data.overview && res.data.overview != '') {
                    movie_data['Overview'] = res.data.overview
                }
                if(res.data.genre_ids > 0) {
                    movie_data['Genre'] = genreIdToName(res.data['genre_ids']).join(', ')
                }
                if(res.data.vote_average) {
                    movie_data['Rating'] = res.data['vote_average']
                }
                if(res.data.release_date && res.data.release_date != null) {
                    movie_data['Release date'] = res.data.release_date
                }

                movieDetails.push(movie_data)
                console.log(movieDetails)
            })
            .catch((e) => {
                throw e
            })
        })
        .catch((e) => console.log(e))
    }
}

function genreIdToName(genre_id_list) {
    let genre_names = []
    for(const genre_id of genre_id_list) {
        genre_names.push(Object.keys(genres).find(key => { return genres[key] == genre_id }))
    }
    return genre_names
}

function genreNameToId() {
    for(const genreName of context.genre) {
        genreId.push(genres[genreName])
    }
}

function nerIdentifiedEntity() {
    if(nerIdentification.people.length == 0 && !nerIdentification.latest && nerIdentification.genre.length == 0 && nerIdentification.movie.length == 0) {
        return false
    } else {
        return true
    }
}

function clearContext() {
    context = {
        people: [],
        genre: [],
        latest: false,
        movie: []
    }
    nerIdentification = {
        people: [],
        genre: [],
        latest: false,
        movie: []
    }
}

function formatMovies(movie) {
    saveName(movie.title)

    let message = {
        'Title': movie.title,
    }
    if(movie.poster_path) {
        message.poster_path = 'http://image.tmdb.org/t/p/w92' + movie.poster_path
    }
    if(movie.genre_ids > 0) {
        message['Genre'] = genreIdToName(movie.genre_ids).join(', ')
    }
    if(movie.release_date && movie.release_date != "") {
        message['Release Date'] = movie.release_date
    }
    if(movie.vote_average) {
        message['TMDB Rating'] = movie.vote_average
    }

    return message
} 

function getFormattedMovies() {
    discoverMovie(async (res) => {
        const movie_list = res.results.slice(0,7)
        for(const movie of movie_list) {
            let message = formatMovies(movie)
            await Promise.all([chat.sendBotMessage(channelId, message)])
        }

        clearContext()
        
        if(movie_discovered) {
            nerManager.load(saved_ner_data)
            movie_discovered = false
            fs.writeFileSync('./utilities/nlp/model.ner', JSON.stringify(nerManager.save()), 'utf8');
        }
    })
}

async function getFormattedPeople() {
    await Promise.all([getPeople(false)])
    .then(() => {
        for(const person of peopleBio) {
            chat.sendBotMessage(channelId, person)
        }
        
        clearContext()

        if(movie_discovered) {
            nerManager.load(saved_ner_data)
            movie_discovered = false
            fs.writeFileSync('./utilities/nlp/model.ner', JSON.stringify(nerManager.save()), 'utf8');
        }
    })
}

async function getFormattedMovieDetails() {
    console.log('in formattedmoviedetails')
    await Promise.all([getMovieDetails()])
    .then(() => {
        console.log("second-",movieDetails)
        for(const movie_data of movieDetails) {
            console.log(movie_data)
            chat.sendBotMessage(channelId, movie_data)
        }
        
        clearContext()
    })
}

async function forceSearch() {
    await axios({
        "method": "GET",
        "url": "https://api.themoviedb.org/3/search/multi?api_key=" + tmdb_api_key + "&language=en-US&query=" + search_for.join() + "&page=1&include_adult=false",
        "headers": {}
    })
    .then(async (res) => {
        if(res.data.results.length == 0){
            chat.sendBotMessage(channelId, "No information found")
        }
        people_in_search_result = 0
        let message = null
        for(result of res.data.results) {
            if(result['media_type'] === 'movie') {
                message = formatMovies(result)
                console.log(message)
            } else if (result['media_type'] === 'person') {
                message = formatPeople(result, null)
                people_in_search_result += 1
            }
            if(message)
                await chat.sendBotMessage(channelId, message)
        }
    })
}

// function initiateConversation(searched_people=false, searched_movie=false) {
//     if(searched_people) {
//         if(peopleBio.length == 1 || people_in_search_result == 1) {
//             await chat.sendBotMessage(channelId, 'Have you seen any of his movies')
//             expect_user_input = true
//         }
//     }
// }

function processText(chnlId, mssg, expect_input, suggest, client_context, callback) {
    channelId = chnlId
    message = mssg
    expect_user_input = expect_input
    suggestion_asked = suggest
    console.log(expect_input, suggest)
    console.log(client_context)

    Promise.all([nlpTextProcessing(), nerTextProcessing()])
    .then(() => {
        
        genreId = []
        peopleId = []

        if((nlp_res['intent'] != 'context.retain' && nlp_res['intent'] != 'context.about') || (client_context && context.people.length >0 && client_context.people != context.people)) {
            //destroy context
            console.log(nlp_res['intent'])
        } else if(nlp_res['intent'] === 'context.retain' || nlp_res['intent'] === 'context.about') {
            console.log('now retain')
            try{
                if(context.movie.length == 0 && context.people.length == 0) {
                    context.people = client_context.people
                }
            } catch(e) {
                nlp_res.answer = "Sorry, I couldn't get the context here"
                context = {
                    people: [],
                    genre: [],
                    latest: false,
                    movie: []
                }
            }
        }
        console.log(nlp_res,ner_res)

        if(/*nlp_res['intent'] === 'None' && */nerIdentifiedEntity()) {
            if(ner_res['entity'] === 'people' || ner_res['entity'] === 'movie' || (context.people.length > 0 || context.movie.length > 0) && (nlp_res['intent'] === 'None'))
            {
                nlp_res['intent'] = 'context.about'
                delete nlp_res['answer']
            }
            suggestion_asked = true
        }

        console.log(context.movie, context.genre, context.latest, context.people)
        console.log(nerIdentifiedEntity())
        if(!nerIdentifiedEntity() && nlp_res['intent'] === 'None') {
            chat.sendBotMessage(channelId, nlp_res.answer)
            console.log(expect_user_input, suggestion_asked)
            callback(expect_user_input, suggestion_asked, {people: context.people})
        } else if(!nerIdentifiedEntity() && nlp_res['intent'] != 'None') {
            console.log(expect_user_input, suggestion_asked)
            if(nlp_res['intent'] === 'context.search' && search) {
                forceSearch(search_for[0])
            } else if(suggestion_asked && !expect_user_input) {
                console.log('leaking yes')
                getFormattedMovies()
                suggestion_asked = false
                console.log(expect_user_input, suggestion_asked)
                callback(expect_user_input, suggestion_asked, {people: context.people})
            } else {
                chat.sendBotMessage(channelId, nlp_res.answer)
                console.log(expect_user_input, suggestion_asked)
                callback(expect_user_input, suggestion_asked, {people: context.people})
            }
        } else if (nerIdentifiedEntity()) {
            if(suggestion_asked) {
                if(context.genre.length > 0 || context.latest || expect_input || nlp_res['intent'].includes('suggest')) {
                    console.log('leaking yes2')
                    getFormattedMovies()
                    suggestion_asked = false
                    console.log(expect_user_input, suggestion_asked)
                    callback(expect_user_input, suggestion_asked, {people: context.people})
                    
                } else {
                    if(!expect_user_input && nlp_res['intent'] === 'context.about') {
                        let save_context = null
                        if(context.movie.length > 0){
                            console.log('movie details')
                            getFormattedMovieDetails()
                            
                        } else if (context.people.length > 0) {
                            getFormattedPeople()
                            save_context = {people: context.people}
                        }
                        suggestion_asked = false
                        console.log(expect_user_input, suggestion_asked)
                        callback(expect_user_input, suggestion_asked, save_context)
                    } else if(!expect_user_input && nlp_res['intent'] === 'context.search') {
                        getFormattedPeople()
                        save_context = {people: context.people}
                        suggestion_asked = false
                        callback(expect_user_input, suggestion_asked, save_context)
                    }
                }
            }
        }
        
    })
}

// processText('ch11', 'show me brad pitt movies')
// genreIdToName([28,12])
exports.processText = processText