const Mitter = require('@mitter-io/node').Mitter;

const mitter = Mitter.forNode(
    process.env.MITTER_APPLICATION_ID,
    {
      "accessKey":    process.env.MITTER_ACCESS_KEY,
      "accessSecret": process.env.MITTER_ACCESS_SECRET
    }
)

const userAuthClient = mitter.clients().userAuth()
const userClient = mitter.clients().users()
const channelClient = mitter.clients().channels()
const messageClient = mitter.clients().messages()

class ChatOperations {
    async sendBotMessage(channelId, message) {
        let today = new Date();
        var reply_message_obj = {}

        let path_key = Object.keys(message).filter((key) => {return key.endsWith('_path')})
        if(path_key.length > 0) {
            let image_path = message[path_key]
            delete message[path_key]
            reply_message_obj = {
                payloadType: 'mitter.mt.ImageMessage',
                senderId: 'LuBHC-SAbpj-t9x10-GoMMf',
                textPayload: JSON.stringify(message),
                timelineEvents: [
                    {
                        type: 'mitter.mtet.SentTime',
                        eventTimeMs: today.getTime(),
                        subject: 'LuBHC-SAbpj-t9x10-GoMMf'
                    }
                ],
                messageData: [
                    {
                        dataType: "text/uri-list",
                        data: {
                            link: image_path,
                            repr: ["thumbnail"]
                        }
                    }
                ]
            }
        } else {
            reply_message_obj = {
                payloadType: 'mitter.mt.Text',
                senderId: 'LuBHC-SAbpj-t9x10-GoMMf',
                textPayload: JSON.stringify(message),
                timelineEvents: [
                    {
                        type: 'mitter.mtet.SentTime',
                        eventTimeMs: today.getTime(),
                        subject: 'LuBHC-SAbpj-t9x10-GoMMf'
                    }
                ]
            }
        }
        console.log(reply_message_obj)
        let sendMessage = messageClient.sendMessage(channelId, reply_message_obj)
        .catch((e) => {
            // console.log(e)
            throw e
        })

        await sendMessage
        .then(() => {
            console.log('Bot replied')
        }).catch((e) => {throw e})
    }
}

module.exports = ChatOperations