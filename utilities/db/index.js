const sqlite3 = require('sqlite3').verbose();
const sha512 = require('js-sha512').sha512;

var db = null
class DbTransactions {
    _openDb() {
        return new Promise((resolve, reject) => {
            db = new sqlite3.Database('./utilities/db/chasitor_main.db', (err) => {
                if (err) {
                    reject(err);
                }
                console.log('Connected to the SQlite database.');
            });
    
            db.run(`
            CREATE TABLE IF NOT EXISTS users(
                user_id text,
                name text UNIQUE,
                password text)
            `);
            resolve()
        })
    }

    _randomAlphaNumeric() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
        for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    
        return text;
    }

    _createUserId() {
        let userId = ''
        let alpha_numeric_list = []
        for(let i=0; i<4; i++) {
            alpha_numeric_list.push(this._randomAlphaNumeric())
        }
        userId = alpha_numeric_list.join('-')

        return userId
    }

    _getShaPassword(password) {
        return sha512(password)
    }

    insertUser(name, password) {
        return new Promise((resolve, reject) => {
            this._openDb()
            .then(() => {
                let user_id = this._createUserId()
                let sha_password = this._getShaPassword(password)

                db.run(`INSERT INTO users VALUES(?,?,?)`, [user_id, name, sha_password], function(err) {
                    if (err) {
                        reject(err)
                    }
                    // get the last insert id
                    // console.log(`A row has been inserted with rowid ${this.lastID}`);
                    console.log(`User added to DB`);
                    resolve(user_id)
                });
                this._closeDb()
            })
        })
    }

    getUser(name, password) {
        return new Promise(async (resolve, reject) => {
            await this._openDb()
            let sha_password = this._getShaPassword(password)

            db.each(`SELECT user_id FROM users WHERE name=? and password=?`, [name, sha_password], function(err, row) {
                if (err) {
                    reject(err)
                }

                resolve(row.user_id)
            })
            this._closeDb()
        })
    }

    _closeDb() {
        db.close((err) => {
            if (err) {
            return console.error(err.message);
            }
            console.log('Close the database connection.');
        });
    }
}

module.exports = DbTransactions 